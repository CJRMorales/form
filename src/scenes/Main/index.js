import React, { useState } from "react";
import FormAd from "./MainForm";
import UserTable from "./UserTable";
import { nanoid } from 'nanoid';
import moment from "moment";

const Main = () => {
  const [data, setData] = useState(0);

  const [mainToggle, setMainToggle] = useState(false);

  const [_data, set_Data] = useState([
    {
      key: nanoid(),
      fname: 'Test',
      lname: 'Dummy',
      // age: 32,
      Gender: ['male'],
      dob: moment(new Date()).format("LL")
    },
    // {
    //   key: nanoid(),
    //   fname: 'jim',
    //   lname: 'Green',
    //   // age: 42,
    //   Gender: ['female'],
    //   dob: moment(new Date()).format("LL")

    // },
    // {
    //   key: nanoid(),
    //   fname: 'Joe',
    //   lname: 'Black',
    //   // age: 32,
    //   Gender: ['Undefined'],
    //   dob: moment(new Date()).format("LL")
    // }
  ])

  const [loading, setLoading] = useState(false);

  function handleLoadingEvent(value) {
    setLoading(value);
  }

  function handleTableDataEvent(val) {
    set_Data(val);
  }

  function handleAddClick() {
    setData(0)
    setMainToggle(true)
  }

  function handleFormSubmit(event) {
    setData(event);
    setMainToggle(false);
    handleLoadingEvent(true);
  }

  function handleBackClick() {
    setMainToggle(false);
  }

  return mainToggle ?
    <FormAd onSubmit={handleFormSubmit}
      handleBackClick={handleBackClick}
    />
    :
    <UserTable
      _data={_data}
      handleAddClick={handleAddClick}
      data={data}
      handleTableDataEvent={handleTableDataEvent}
      loading={loading}
      handleLoadingEvent={handleLoadingEvent}
    />
};

export default Main;