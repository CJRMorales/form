/* eslint-disable react/prop-types */
import React, { useState } from "react";
import { Form, Input, Button, DatePicker, Card, Select } from "antd";

const { Option } = Select;

function FormAd({ onSubmit, handleBackClick }) {

    const [dateOfBirth, setDateOfBirth] = useState(new Date());

    const handleDateChange = (date) => {
        setDateOfBirth(date);
    };

    React.useEffect(() => {
        let unmounted = false;

        if (unmounted) return;

        setDateOfBirth(0);

        return () => {
            unmounted = true;
        }
    }, []);

    return (
        <div className="wrapper">
            <Card style={{ width: 420, borderRadius: 5, paddingTop: 30 }}>
                <Form
                    name="complex-form"
                    onFinish={onSubmit}
                    labelCol={{ span: 8 }}
                    wrapperCol={{ span: 16 }}
                >
                    <h1
                        style={{
                            display: "flex",
                            justifyContent: "center",
                            alignItems: "center",
                            marginBottom: 35,
                        }}
                    >
                        Survey Form
                    </h1>

                    {/* FirstName */}
                    <Form.Item label="First Name">
                        <Form.Item
                            name="fname"
                            noStyle
                            rules={[{ required: true, message: "First Name is required" }]}
                        >
                            <Input
                                name="firstName"
                                style={{ width: 150, borderRadius: 5 }}
                                placeholder="First Name"
                            />
                        </Form.Item>
                    </Form.Item>

                    {/* LastName */}
                    <Form.Item label="Last Name">
                        <Form.Item
                            name="lname"
                            noStyle
                            rules={[{ required: true, message: "Last Name is required" }]}
                        >
                            <Input
                                name="lastName"
                                style={{ width: 150, borderRadius: 5 }}
                                placeholder="Last Name"
                            />
                        </Form.Item>
                    </Form.Item>

                    {/* Gender */}
                    <Form.Item label="Gender">
                        <Form.Item
                            name="Gender"
                            noStyle
                            rules={[{ required: true, message: "Gender is required" }]}
                        >
                            <Select
                                name="gender"
                                style={{
                                    width: 150,
                                    borderRadius: 5,
                                    textAlign: "left"
                                }}
                                placeholder="Gender"
                            >
                                <Option value="Male">Male</Option>
                                <Option value="Female">Female</Option>
                                <Option value="Undefined">Undefined</Option>
                            </Select>
                        </Form.Item>
                    </Form.Item>

                    {/* dob */}
                    <Form.Item label="Date of Birth" style={{ marginBottom: 0 }}>
                        <Form.Item
                            name="dob"
                            rules={[{ required: true }]}
                            style={{ width: "calc(50% - 8px)" }}
                        >
                            <DatePicker
                                selected={dateOfBirth}
                                onChange={handleDateChange}
                                showTimeSelect
                                style={{ width: 150, borderRadius: 5, marginRight: 201 }}
                            />
                        </Form.Item>
                    </Form.Item>

                    {/* Submit */}
                    <Form.Item label=" " colon={false}>
                        <Button
                            // onClick={onSubmit && handleBackClick}
                            type="primary"
                            htmlType="submit"
                            style={{ width: 150, borderRadius: 5 }}
                        >
                            Submit
                        </Button>

                    </Form.Item>
                    <Button
                        // icon={<IoIosArrowBack />}
                        size={'large'}
                        shape={'round'}
                        style={{ height: 40, marginTop: 10 }}
                        onClick={handleBackClick}
                    >Back</Button>
                    {/* <Button
                        size={'small'}
                        shape={'round'}
                        style={{ width: 150, marginTop: 10, color: 'purple' }}
                        onClick={handleRefreshClick}
                    >Dev-Only: Refresh
                    </Button> */}
                </Form>
            </Card>
        </div>
    );
}

export default FormAd;
