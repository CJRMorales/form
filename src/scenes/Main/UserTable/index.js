/* eslint-disable react/prop-types */
import React from 'react'
import { Table, Tag, Button } from 'antd';
import { PlusOutlined, SyncOutlined } from '@ant-design/icons';
import { nanoid } from 'nanoid';
import moment from 'moment';
import Service from '../../../services/BackendService';

export default function UserTable({
  handleAddClick,
  data,
  _data,
  handleTableDataEvent,
  loading,
  handleLoadingEvent,
}) {
  const columns = [
    {
      title: 'First Name',
      dataIndex: 'fname',
      key: 'fname',
      render: (text) => <p>{text}</p>,
    },
    {
      title: 'Last Name',
      dataIndex: 'lname',
      key: 'lname',
      render: (text) => <p>{text}</p>,
    },
    // {
    //     title: 'Age',
    //     dataIndex: 'age',
    //     key: 'age',
    // },
    {
      title: 'Gender',
      key: 'Gender',
      dataIndex: 'Gender',
      render: (Gender) => (
        <>
          {Gender.map((tag) => {
            let color = tag.length > 5 ? 'indianred' : 'lightblue';
            if (tag === 'Undefined') {
              color = 'yellow';
            }
            return (
              <Tag color={color} key={tag}>
                {tag.toUpperCase()}
              </Tag>
            );
          })}
        </>
      ),
    },
    {
      title: 'Date of Birth',
      dataIndex: 'dob',
      key: 'dob',
    },

    // {
    //     title: 'Action',
    //     key: 'action',
    //     render: (text, record) => (
    //         <Space size="middle">
    //             <Button danger>Delete</Button>
    //         </Space>
    //     ),
    // },
  ];

  React.useEffect(() => {
    let unmounted = false;

    Service.create(data).then(({ data }) => {
      if (unmounted) return;
      if (data === 0) return;
      const newObj = {
        key: nanoid(),
        fname: data.fname,
        lname: data.lname,
        Gender: [data.Gender],
        dob: moment(data.dob).format('LL'),
      };
      handleTableDataEvent([..._data, newObj]);
      handleLoadingEvent(false);
    });
    return () => {
      unmounted = true;
    };
  }, []);

  return (
    <div className="wrapper">
      <Table
        className="tableShadow"
        columns={columns}
        dataSource={_data}
        pagination={{ pageSize: 4 }}
        style={{ borderRadius: 7 }}
      />
      <Button
        onClick={handleAddClick}
        type="primary"
        style={{ width: 165, height: 65 }}
        shape="round"
      >
        {loading ? (
          <SyncOutlined spin style={{ fontSize: '34px' }} />
        ) : (
          <PlusOutlined style={{ fontSize: '28px' }} />
        )}
      </Button>

      {/* <Button
                onClick={handleUpdateRowChange}
                style={{ height: 65 }}
                shape="square"
                danger
            >Testing Only
                <PlusOutlined style={{ fontSize: '28px', color: 'red' }} />
            </Button> */}
    </div>
  );
}
