const Service = {
  index() {
    return new Promise((resolve) => {
      setTimeout(() => resolve([]), 1500);
    });
  },
  create(data) {
    return new Promise((resolve) => {
      setTimeout(() => resolve({ data }), 1500);
    });
  },
};

export default Service;
